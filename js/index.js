let currentBoard = null;
let keysOfBoadsList = [];
let boardsName = new Set();
let selectedBorderName;

let blockedColorList = [
    "#efefef",
    "#feaeae",
    "#cdfcb6",
    "#b6d7fc",
    "#e2e2e2"
];

class Note {

    constructor (noteText, noteColor, x, y, width, height) {
        this.m_noteText = noteText;
        this.m_noteColor = noteColor;
        this.m_x = x;
        this.m_y = y;
        this.m_width = width;
        this.m_height = height;
    }

    getColor () {
        return this.m_noteColor;
    }

    getText () {
        return this.m_noteText;
    }

    getX () {
        return this.m_x;
    }

    getY () {
        return this.m_y;
    }

    getWidth () {
        return this.m_width;
    }

    getHeigth () {
        return this.m_height;
    }

    setText (newText) {
        this.m_noteText = newText;
    }

    setColor (newColor) {
        this.m_noteColor = newColor;
    }

    setX (x) {
        this.m_x = x;
    }

    setY (y) {
        this.m_y = y;
    }

    setWidht (width) {
        this.m_width = width;
    }

    wetHeight (height) {
        this.m_height = height;
    }
}

class Board {

    constructor (boardName, boardColor, noteList) {
        this.boardName = boardName;
        this.boardColor = boardColor;
        this.noteList = noteList;
        this.noteIndex = 0;
    }

    getBoardName () {
        return this.boardName;
    }

    getBoardColor () {
        return this.boardColor;
    }

    getNoteList () {
        return this.noteList;
    }

    getNoteIndex () {
        return this.noteIndex;
    }

    setBoardName (boardName) {
        this.boardName = boardName;
    }

    setBoardColor (boardColor) {
        this.boardColor = boardColor;
    }

    setNoteList (noteList) {
        this.noteList = noteList;
    }

    setNoteIndex (noteIndex) {
        this.noteIndex = noteIndex;
    }
}

showAllBoards();
if(keysOfBoadsList.length > 0)
    showSelectedBoard(keysOfBoadsList[0]);

// this function is used to store boardes in local storage
function storeAtLocalStorage () {
    const jsonString = JSON.stringify(keysOfBoadsList);
    localStorage.setItem('keysOfBoads', jsonString);
}

// this function is used to store specific Board in local storage
function saveBoard () {
    const boardJsonString = JSON.stringify(currentBoard);
    localStorage.setItem(currentBoard.getBoardName(), boardJsonString);
}

// this function is used to delete specific Board from local storage
function removeBoard (boardName) {
    let currIndex = keysOfBoadsList.indexOf(boardName);
    boardsName.delete(boardName);
    keysOfBoadsList.splice(currIndex, 1);
    storeAtLocalStorage();
    localStorage.removeItem(boardName);
}

// this function is used to retrieve specific board from local storage
function getSpecificBoardFromLocalStorage (boardID) {
    const board = localStorage.getItem(boardID);
    currentBoard = JSON.parse(board);
    Object.setPrototypeOf(currentBoard, Board.prototype);
    for (nextNote of currentBoard.getNoteList()) {
        Object.setPrototypeOf(nextNote, Note.prototype);
    }
}

// this function is used to retrieve boards keys from local storage
function getFromLocalStorage() {
    const keysOfBoadsStorage = localStorage.getItem('keysOfBoads');
    keysOfBoadsList =  JSON.parse(keysOfBoadsStorage) || [];
}

// this function is used to show all boards in the nav bar
function showAllBoards() {
    getFromLocalStorage();
    let allBoards = document.getElementById('all-boards');
    allBoards.innerHTML = null;
    for(let i=0;i<keysOfBoadsList.length;i++) {
        boardsName.add(keysOfBoadsList[i]);
        let currentBoardHTML = `<button class="tab" id="${keysOfBoadsList[i]}" onclick="showSelectedBoard('${keysOfBoadsList[i]}')">${keysOfBoadsList[i]}</button>`;
        allBoards.innerHTML += currentBoardHTML;
    }
}

// this function is used to highlight the selected board at nav bar and load it is content.
function showSelectedBoard(selectedBorderID) {
    if (selectedBorderName != null && document.getElementById(selectedBorderName) != null) {
        document.getElementById(selectedBorderName).className = "tab";
    }

    selectedBorderName = selectedBorderID;
    getSpecificBoardFromLocalStorage(selectedBorderID);
    document.getElementById('board-content-root').innerHTML =`<div class="board-content" id="board-content">
                                                             </div>
                                                             <div class="board-editor">
                                                                <img class="edit-board" onclick="showEditBoardPopUp();" src="img/edit-button.png">
                                                                <img class="delete-board" onclick="deleteBoard();" src="img/delete-button.jpg"></img>
                                                             </div>`;

    if (currentBoard == null) {
        document.getElementById(selectedBorderID).className = "tab";
        return;
    }

    document.getElementById(selectedBorderID).className= "tab-clicked";
    document.getElementById('board-content').style.background = currentBoard.boardColor;
    loadNotesForSelectedBoard(currentBoard, currentBoard.getNoteList());
}

// this function is used to enable user adding a new board
function addNewBoard() {

    let name = document.getElementById('board-name-pop-up').value;
    let color = document.getElementById('board-color-pop-up').value;
    if (name.trim() == "") {
        alert("board name was empty")
        return;
    }
    if (boardsName.has(name) == 1) {
        alert("board name was already exist");
        return;
    }

    for (bColor of blockedColorList) {
        if (bColor == color) {
            alert("This selected Board color is blocked. Try another one.");
            return;
        }
    }

    boardsName.add(name);
    keysOfBoadsList.push(name);
    currentBoard = new Board(name, color, []);
    storeAtLocalStorage();
    saveBoard();
    showAllBoards();
    hideAddBoardPopUp();
    showSelectedBoard(name);
}

// this function is used to enable user deleting current board
function deleteBoard() {

    getSpecificBoardFromLocalStorage(selectedBorderName);
    removeBoard(selectedBorderName);

    showAllBoards();
    if (keysOfBoadsList == null || keysOfBoadsList.length == 0) {
        document.getElementById('board-content-root').innerHTML = null;
        return;
    }
    if(keysOfBoadsList.length>0)
        showSelectedBoard(keysOfBoadsList[0]);

}

// this function is used to enable user editing current board info
function editBoard() {
    getSpecificBoardFromLocalStorage(selectedBorderName);
    let newName = document.getElementById('board-name-pop-up').value;
    let newColor = document.getElementById('board-color-pop-up').value;
    let currIndex = keysOfBoadsList.indexOf(currentBoard.getBoardName());

    if (newName.trim() == "") {
        alert("board name was empty")
        return;
    }
    if(currentBoard.getBoardName() == newName)
        ;
    else if (boardsName.has(newName) == 1) {
        alert("board name was already exist");
        return;
    }

    if (currentBoard.getBoardName() != newName) {
        boardsName.delete(currentBoard.getBoardName());// boardsName now was not sorted
        boardsName.add(newName);
        removeBoard(currentBoard.getBoardName());
        currentBoard.setBoardName(newName);
        keysOfBoadsList.splice(currIndex, 0, newName);
        storeAtLocalStorage();
    }

    currentBoard.setBoardColor(newColor);
    saveBoard();
    document.getElementById('board-content').style.background = newColor;
    showAllBoards();
    hideAddBoardPopUp();
    showSelectedBoard(newName);
}

// when user want to edit board, the updated info shall filled in a pop up appeared
function showEditBoardPopUp() {

    let modal = document.getElementById("myModal");
    modal.style.display = "block";
    document.getElementById('myModal').innerHTML = `<div class="close-container">
                                                        <span class="close" onClick="hideAddBoardPopUp()">&times;</span>
                                                    </div>
                                                    <div class="modal-content">
                                                        <div class="board-information">
                                                            <span>
                                                                Board Name: <input type="text" id="board-name-pop-up" maxlength="15" value="${currentBoard.getBoardName()}">
                                                            </span>
                                                            <span>
                                                                Board Color: <input type="color" id="board-color-pop-up" value="${currentBoard.getBoardColor()}">
                                                            </span>
                                                            <hr>
                                                            <span>
                                                                <button type="button" class="save-btn" onClick="editBoard()">Edit</button>
                                                                <button type="button" class="cancel-btn" onClick="hideAddBoardPopUp()">CANCEL</button>
                                                            </span>
                                                        </div>
                                                    </div>`;
}

// when user want to add a new board, board info shall filled in a pop up appeared
function showAddBoardPopUp () {

    let modal = document.getElementById("myModal");
    modal.style.display = "block";
    document.getElementById('myModal').innerHTML = `<div class="close-container">
                                                        <span class="close" onClick="hideAddBoardPopUp()">&times;</span>
                                                    </div>
                                                    <div class="modal-content">
                                                        <div class="board-information">
                                                            <span>
                                                                Board Name: <input type="text" id="board-name-pop-up" maxlength="15">
                                                            </span>
                                                            <span>
                                                                Board Color: <input type="color" id="board-color-pop-up" >
                                                            </span>
                                                            <hr>
                                                            <span>
                                                                <button type="button" class="save-btn" onClick="addNewBoard()">SAVE</button>
                                                                <button type="button" class="cancel-btn" onClick="hideAddBoardPopUp()">CANCEL</button>
                                                            </span>
                                                        </div>
                                                    </div>`;
}

// this function used to hide pop-ups related to boards info
function hideAddBoardPopUp () {

    if ( document.getElementById("myModal") != null) {
        let modal = document.getElementById("myModal");
        modal.style.display = "none";
    }
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {

    let modal = document.getElementById("myModal");
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

function createNewNoteHTML (note, noteIndex) {
    if (note.getText() == null) {
        note.setText("");
    }

    return `<div id="note${noteIndex}">
                <div class="note-content">
                    <textarea class="note-content-p" id="note${noteIndex}-content"> ${note.getText()} </textarea>
                </div>
                <div class="note-tile-bottom-bar">
                    <div class="color-list">
                        <span class="note-color grey" id="note${noteIndex}-color1">
                        </span>
                        <span class="note-color pink" id="note${noteIndex}-color2">
                        </span>
                        <span class="note-color green" id="note${noteIndex}-color3">
                        </span>
                        <span class="note-color blue" id="note${noteIndex}-color4">
                        </span>
                    </div>
                    <div class="note-remove-button-root">
                        <span class="note-remove-button" id="note${noteIndex}-remove-button">
                            x
                        </span>
                    </div>
                </div>
            </div>`;
}

function createNote (noteList, note, boardNoteIndex) {
    let noteIndex = noteList.indexOf(note);
    let newNoteHTML = document.createElement('div');
    let noteBoard = document.getElementById("board-content");

    newNoteHTML.className = "note-tile";
    newNoteHTML.innerHTML = createNewNoteHTML(noteList[noteIndex], boardNoteIndex);
    newNoteHTML.style.left = note.getX() + "px";
    newNoteHTML.style.top = note.getY() + "px";
    noteBoard.appendChild(newNoteHTML);

    newNoteHTML.style.backgroundColor = note.getColor();
    newNoteHTML.style.boxShadow = "0px 0px 12px " + note.getColor();

    let removeButton = document.getElementById("note" + boardNoteIndex + "-remove-button");
    removeButton.addEventListener("click", function(e) {
        newNoteHTML.parentElement.removeChild(newNoteHTML);
        noteIndex = noteList.indexOf(note);
        noteList.splice(noteIndex, 1);
        saveBoard();
    });

    let noteTextArea = document.getElementById("note" + boardNoteIndex + "-content");
    noteTextArea.addEventListener("change", function(e) {
        note.setText(noteTextArea.value);
        saveBoard();
    });

    let noteColor1 = document.getElementById("note" + boardNoteIndex + "-color1");
    noteColor1.addEventListener("click", function(e) {
        note.setColor("#EFEFEF");
        newNoteHTML.style.backgroundColor = "#EFEFEF";
        newNoteHTML.style.boxShadow = "0px 0px 12px #EFEFEF";
        saveBoard();
    });
    let noteColor2 = document.getElementById("note" + boardNoteIndex + "-color2");
    noteColor2.addEventListener("click", function(e) {
        note.setColor("#FEAEAE");
        newNoteHTML.style.backgroundColor = "#FEAEAE";
        newNoteHTML.style.boxShadow = "0px 0px 12px #FEAEAE";
        saveBoard();
    });
    let noteColor3 = document.getElementById("note" + boardNoteIndex + "-color3");
    noteColor3.addEventListener("click", function(e) {
        note.setColor("#CDFCB6");
        newNoteHTML.style.backgroundColor = "#CDFCB6";
        newNoteHTML.style.boxShadow = "0px 0px 12px #CDFCB6";
        saveBoard();
    });
    let noteColor4 = document.getElementById("note" + boardNoteIndex + "-color4");
    noteColor4.addEventListener("click", function(e) {
        note.setColor("#B6D7FC");
        newNoteHTML.style.backgroundColor = "#B6D7FC";
        newNoteHTML.style.boxShadow = "0px 0px 12px #B6D7FC";
        saveBoard();
    });
    let isDown = false;
    let offset =[0, 0];
    let mousePosition;

    newNoteHTML.addEventListener('mousedown', function(e) {
        isDown = true;
        offset = [
            newNoteHTML.offsetLeft - e.clientX,
            newNoteHTML.offsetTop - e.clientY
        ];
    }, true);

    newNoteHTML.addEventListener('mouseup', function() {
        isDown = false;
    }, true);

    document.addEventListener('mousemove', function(event) {
        event.preventDefault();
        if (isDown) {
            mousePosition = {
                x : event.clientX,
                y : event.clientY
            };

            if (mousePosition.x + offset[0] >= 0 && mousePosition.y + offset[1] >= 0) {
                note.setX(mousePosition.x + offset[0]);
                note.setY(mousePosition.y + offset[1]);
                newNoteHTML.style.left = (mousePosition.x + offset[0]) + 'px';
                newNoteHTML.style.top  = (mousePosition.y + offset[1]) + 'px';
                saveBoard();
            }
        }
    }, true);

}

// this function is used to enable user creating a new note on current board.
function createNewNote () {
    if (currentBoard == null) {
        alert("First, select/create new board.")
        return;
    }
    let newNote = new Note("", "#EFEFEF", Math.floor(Math.random() * 1600) % 1597, Math.floor(Math.random() * 700) % 691, 252, 226);
    currentBoard.getNoteList().push(newNote);
    saveBoard();

    createNote(currentBoard.getNoteList(), newNote, currentBoard.getNoteIndex());
    currentBoard.setNoteIndex(currentBoard.getNoteIndex() + 1);
}

function loadNotesForSelectedBoard(board, noteList) {
    if (noteList == null || board == null) {
        return;
    }
    let noteBoard = document.getElementById("board-content");

    noteBoard.innerHTML = "";
    board.setNoteIndex(0);
    for (let i = 0; i < noteList.length; i++) {
        createNote(noteList, noteList[i], board.getNoteIndex());
        board.setNoteIndex(board.getNoteIndex() + 1);;
    }
}
